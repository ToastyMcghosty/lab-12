#include <iostream>
using namespace std;

class FootballPlayer
{
private:
	string name;
public:
	FootballPlayer(const string& name)
	{ this->name = name; }
	string getName() const
	{ return name; }
};

class FootballPlayerList
{
private:
	FootballPlayer** list;
	unsigned size;
	unsigned maxSize;
public:
	FootballPlayerList() : FootballPlayerList(100)
	{
	}

	FootballPlayerList(int maxSize)
	{
		this->maxSize = maxSize;
		size = 0;
		list = new FootballPlayer*[maxSize];
	}

	~FootballPlayerList()
	{
		for (unsigned i = 0; i < size; i++)
			delete list[i];
		delete [] list;
	}

	void add(FootballPlayer* player)
	{
		if (size == maxSize) return;
		list[size++] = player;
	}

	FootballPlayer* getElement(int index) const
	{ return list[index]; }

	int getSize() const
	{ return size; }

	void replace(int index, FootballPlayer* player)
	{ list[index] = player; }

	void print(const int index) const
	{
		cout << getElement(index)->getName() << endl;
	}

	void setMaxSize(const int maxSize)
	{ this->maxSize = maxSize; }

};

int main()
{
	FootballPlayerList list;
	list.add(new FootballPlayer("Mahomes"));
	list.add(new FootballPlayer("Kelce"));
	list.add(new FootballPlayer("Pacheco"));

	list.print(2);
	list.replace(2, new FootballPlayer("Person"));
	list.print(2);

	cout << endl;
	for (int i = 0; i < list.getSize(); i++)
		list.print(i);

	//terminate
	return 0;

}
